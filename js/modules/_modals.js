//////////////////////////////////////////////////////////
////  Modals
//////////////////////////////////////////////////////////

const Modals = (() => {

	let DEBUG = false;
	let INFO = { name : 'Modals', version : 2.5 };

	//////////////////////////////////////////////////////////
  ////  Newsletter
  //////////////////////////////////////////////////////////

	const newsletter = {

  	targetElementID: "modal--popup-newsletter",

  	cookie: {
    	name: "elevai--popup-newsletter",
      value: "seen",
  	},

  	delay: function() {
    	return parseInt( $( '#' + this.targetElementID ).attr( 'data-timeout-delay-milliseconds' ) ) || 3000;
  	},

  	expires: function() {
    	return parseInt( $( '#' + this.targetElementID ).attr( 'data-cookie-expires-days' ) ) || 60;
  	},

  	setCookie: function() {
    	Browser.setCookie( this.cookie.name, this.cookie.value, this.expires() );
    },

    show: function( $delay ) {

      let target = this.targetElementID;
      let setCookie = this.setCookie();

      setTimeout(function(){
        MicroModal.show( target, {
          awaitCloseAnimation: true,
          onShow: function(modal) {
            console.log("micromodal opened");
          },
          onClose: function(modal) {
            console.log("micromodal closed");
            setCookie;
          }
        });
      }, $delay );

    },

    init: function() {

      let cookieNotSeen = Browser.getCookie( this.cookie.name ) !== "seen";
      let target = this.targetElementID;
      let modalExists = $("#" + target).length;

      $( "#" + target + " [data-micromodal-close]" ).on("click", function( event ){
        console.log( 'clicked!' );
        MicroModal.close( target );
  		});

      if ( modalExists && cookieNotSeen ) {
  		  this.show( this.delay() );
  		}

    },

	};

	//////////////////////////////////////////////////////////
  ////  Main
  //////////////////////////////////////////////////////////

  const main = () => {
    newsletter.init();
  };

	//////////////////////////////////////////////////////////
	//// Init
	//////////////////////////////////////////////////////////

	const init = () => {
		main();
	};

	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////

	return {
    init : init
	};

})();
