//////////////////////////////////////////////////////////
////  Sliders
//////////////////////////////////////////////////////////

const Sliders = (() => {

	let DEBUG = true;
	let INFO = { name : 'Sliders', version : 1.0 };
	let GLIDERS = {};
	let BREAKPOINTS = {
		'xs' : 567,
		'sm' : 768,
		'md' : 992,
		'lg' : 1200,
		'xlg' : 1360
	};

	//////////////////////////////////////////////////////////
	////  Gallery
	//////////////////////////////////////////////////////////

  const category = () => {

    $('.js--slider').each(function(){

      let thisID = $(this).attr('id');
      let thisElement = '#' + thisID;

      let options = {
      	type: 'carousel',
      	perView: 1,
      	gap: 0,
      	peek: {
        	before: 0,
        	after: 80,
      	},
      	breakpoints: {
        	992: {
          	peek: {
            	before: 0,
            	after: 0,
          	}
        	}
      	},
    	};

      let thisSlider = new Glide( thisElement, options ).mount();

      GLIDERS[thisID] = thisSlider;

      setTimeout(function(){
        thisSlider.update();
      }, 250 );

      thisSlider.on('build.after', function() {
        var slideHeight = $('.glide__slide--active').outerHeight();
        var glideTrack = $('.glide__track').outerHeight();
        if (slideHeight != glideTrack) {
          var newHeight = slideHeight;
          $('.glide__track').css('height', newHeight);
        }
      });

      thisSlider.on('run.after', function() {
        var slideHeight = $('.glide__slide--active').outerHeight();
        var glideTrack = $('.glide__track').outerHeight();
        if (slideHeight != glideTrack) {
          var newHeight = slideHeight;
          $('.glide__track').css('height', newHeight);
        }
      });

    });

    $(".glide__button").on("click", function(event){
      let thisGlideContainerID = $(this).closest('.js--slider').attr('id') || false;
      let thisAction = $(this).attr('data-action') || false;
      if ( thisAction && thisGlideContainerID ) {
        GLIDERS[thisGlideContainerID].go( thisAction );
      }
    });

  };

	//////////////////////////////////////////////////////////
	////  Main
	//////////////////////////////////////////////////////////

  const main = () => {
    category();
  };

	//////////////////////////////////////////////////////////
	////  Public Method | Initialize
	//////////////////////////////////////////////////////////

	const init = () => {
    main();
	};

	//////////////////////////////////////////////////////////
	////  Returned Methods
	//////////////////////////////////////////////////////////

	return {
		info : INFO,
		init : init
	};

})();
