///////////////////////////////////////////////////////////////////////////////////////////////
////  Vendor Imports
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend quiet "../node_modules/jquery/dist/jquery.min.js";
// @codekit-prepend quiet "../node_modules/bootstrap/dist/js/bootstrap.min.js";
// @codekit-prepend quiet "../node_modules/micromodal/dist/micromodal.min.js";
// @codekit-prepend quiet "../node_modules/shopify-cartjs/dist/rivets-cart.min.js";
// @codekit-prepend quiet "../node_modules/@glidejs/glide/dist/glide.min.js";
// @codekit-prepend quiet "../node_modules/validator/validator.min.js";
// @codekit-prepend quiet "../node_modules/aos/dist/aos.js";

///////////////////////////////////////////////////////////////////////////////////////////////
////  Polite Department Imports
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend "./modules/_credits.js";
// @codekit-prepend "./modules/_browser.js";
// @codekit-prepend "./modules/_forms.js";
// @codekit-prepend "./modules/_scrolling.js";
// @codekit-prepend "./modules/_sliders.js";
// @codekit-prepend "./modules/_modals.js";

//////////////////////////////////////////////////////////////////////////////////////////
////  Execute Scripts - Immediately!
//////////////////////////////////////////////////////////////////////////////////////////

$(function(){

  Credits.init();

}());

//////////////////////////////////////////////////////////////////////////////////////////
////  Execute Scripts - When Document Ready!
//////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function(){

  // Cart JS
  CartJS.init( Theme.cart );

  // Scrolling Status
  Scrolling.init();

  // Modals
  Modals.init();

  // Sliders
  Sliders.init();

  // Sliders
  Forms.init();

  $('.collapsible__content').on('hide.bs.collapse', function () {
    $(this).closest('.collapsible').find('.collapsible__button-copy').html('Expand Bio');
  });

  $('.collapsible__content').on('show.bs.collapse', function () {
    $(this).closest('.collapsible').find('.collapsible__button-copy').html('Collapse Bio');
  });

  // Animate on scroll
  AOS.init();

});

//////////////////////////////////////////////////////////
////  Cart Ready
//////////////////////////////////////////////////////////

$(document).on('cart.ready', function( event, cart ) {

  let debugThis = false;
  let test = false;

  if ( debugThis ) {
    console.log( 'cart.ready:' );
    console.log( [event, cart] );
  }

});

//////////////////////////////////////////////////////////
////  Cart Request Started
//////////////////////////////////////////////////////////

$(document).on('cart.requestStarted', function( event, cart ) {

  let debugThis = false;

  if ( debugThis ) {
    console.log( 'cart.requestStarted:' );
    console.log( [event, cart] );
  }

});

//////////////////////////////////////////////////////////
////  Cart Request Complete
//////////////////////////////////////////////////////////

$(document).on( 'cart.requestComplete', function( event, cart ) {

  let debugThis = false;

  if ( debugThis ) {
    console.log( 'cart.requestComplete:' );
    console.log( [event, cart] );
  }

});
